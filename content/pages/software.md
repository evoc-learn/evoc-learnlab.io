title: evoc-learn Software
author: evoc-learn
save_as: software.html
url: software

# VocalTractLab {#vtl}

![VTL](images/software/vtl.png)

The state-of-the-art articulatory synthesiser VocalTractLab (version 2.3) is available at [vocaltractlab.de](https://vocaltractlab.de/download-vocaltractlab/VTL2.3.zip)

You may also be interested in these source repositories which separate the graphical components and provide solutions for GNU/Linux: [API](https://github.com/TUD-STKS/VocalTractLabBackend-dev), [GUI](https://github.com/TUD-STKS/VocalTractLab-dev).

---


# PyVTL {#PyVTL}

A set of Python bindings for VocalTractLab makes it easy to setup experiments using Numpy, Pandas, SciPy, Praat, and Matplotlib for visualisation:

![Microprosody](images/software/microprosody.png)

Find the code and experimental setup for an analysis of microprosody here: [github.com/TUD-STKS/Microprosody](https://github.com/TUD-STKS/Microprosody)

---


# Evoclearn {#evl}

 ![Evoclearn sim](images/software/evlsim.png)

 - The above simulation of vocal learning with vocal tracts of different sizes is presented here: [gitlab.com/Anqi_Xu/evoc_learn](https://gitlab.com/Anqi_Xu/evoc_learn)

 ![Evoclearn opt](images/software/evlopt.png)
 
 - Vocal exploration is implemented as an optimisation process, as illustrated above, by the following Python packages: [evoclearn-core](https://gitlab.com/evoc-learn-group/evoc-learn-core), [evoclearn-rec](https://gitlab.com/evoc-learn-group/evoc-learn-rec), [evoclearn-opt](https://gitlab.com/evoc-learn-group/evoc-learn-opt)
 
 - These packages can be installed from the *Python Package Index* and a complete example is available as a Python notebook: [Quickstart Guide](https://tinyurl.com/evoclearn)
