title: evoc-learn Events
author: evoc-learn
save_as: events.html
url: events

# Interspeech 2022 {#is2022}

Three members of the team travelled to *Incheon, South Korea* in September for *Interspeech 2022* where we chaired and presented at the following sessions:

 ![Speech Production](images/is2022/is20220919.jpg)

 - **Speech Production** on Monday the 19th: ***Exploration strategies for articulatory synthesis of complex syllable onsets*** \[[slides](docs/dvn_is2022_optccv_slides.pdf), [presentation](https://youtu.be/bDDHYDZkDmI), [samples](https://danielshaps.github.io/#is2022-1)\]
 
 ![Speech Synthesis New Applications](images/is2022/is20220920.jpg)
 
 - **Speech Synthesis: New Applications** on Tuesday the 20th: ***Articulatory Synthesis for Data Augmentation in Phoneme Recognition***
 
 ![Show and Tell III](images/is2022/is20220921.jpg)
 
 - **Show and Tell III** on Wednesday the 21st: ***Evoc-Learn — High quality simulation of early vocal learning*** \[[poster](docs/xu_is2022_evoclearn_poster.pdf)\]

---


# Project Workshop in Macedonia {#workshop}

In July 2022, Branislav Gerazov hosted a Workshop at *Lake Ohrid in Macedonia* where the team presented and reflected on aspects of the project.

![Workshop Ohrid Macedonia](images/workshop/ohrid.png)

The following talks were presented:

 - **Yi Xu** -- *High Quality Simulation of Early Vocal Learning* \[[presentation](https://youtu.be/l3Qw5GtptUQ)\]
 - **Santitham Prom-on** -- *Learning of Thai Syllables* \[[presentation](https://youtu.be/M5mQRDBYfyM)\]
 - **Peter Birkholz** -- *VocalTractLab Features and Roadmap* \[[presentation](https://youtu.be/PWbNma8PLkc)\]
 - **Daniel van Niekerk** -- *Learning CVs and CCVs* \[[presentation](https://youtu.be/WnaiGEyBu8c)\]
 - **Anqi Xu** -- *A computational simulation of human vocal learning* \[[presentation](https://youtu.be/yzwyK2T0Oxs)\]
 - **Paul K. Krug** -- *Artificial Vocal Learning guided by Phoneme Recognition and Visual Information* \[[presentation](https://youtu.be/40cLJ_BucY4)\]
 - **Branislav Gerazov** -- *ASR for early vocal learning* \[[presentation](https://youtu.be/6ZQPRTDkB8o)\]
 
![Workshop Ohrid Zoom](images/workshop/ohrid_zoom.png)

---


# Interspeech 2021 {#is2021}

Anqi Xu presented an analysis of the relationship between articulatory and acoustic spaces for British vowels at *Interspeech 2021*:

 - ***Model-based exploration of linking between vowel articulatory space and acoustic space*** \[[samples](http://www.homepages.ucl.ac.uk/~uclyyix/EVL/hVd/)\]
 
---

# Interspeech 2020 {#is2020}

Daniel van Niekerk presented a paper at *Interspeech 2020* testing the use of automatic speech recognition (ASR) as an evaluator of articulatory-produced CV syllables discovered using different optimisation algorithms:

 - ***Finding intelligible consonant-vowel sounds using high-quality articulatory synthesis*** \[[slides](docs/dvn_is2020_optcv_slides.pdf), [presentation](https://youtu.be/AWwZZKbSy04)\]
 
