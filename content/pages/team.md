title: evoc-learn Team
author: evoc-learn
save_as: team.html
url: team


# The evoc-learn Team

Project title 	|  EVoc-Learn: High quality simulation of early vocal learning
---|---
Funded by |	[Leverhulme Trust](https://www.leverhulme.ac.uk/) Research Project Grant RPG-2019-241
PI  |  [prof. Yi Xu](http://www.homepages.ucl.ac.uk/~uclyyix/), Department of Speech, Hearing and Phonetic Sciences, [University College London](https://www.ucl.ac.uk/), London, UK
Project Partners  |  <ul><li>Peter Birkholz, [Technical University Dresden](https://tu-dresden.de/), Dresden, Germany </li><li> dr. Santitham Prom-on, [King Mongkut's University of Technology Thonburi](https://www.cpe.kmutt.ac.th/en/), Bangkok, Thailand </li><li> dr. Lorna Halliday [University of Cambridge](https://www.cam.ac.uk/), Cambridge, UK </li></ul>
Research Assistants  | <ul> <li> Anqi Xu, University College London, UK </li> <li> Daniel van Niekerk, University College London, UK </li><li> Paul Konstantin Krug, Technical University Dresden, Germany </li><li> [Brаnislаv Gеrаzоv](https://gerazov.github.io/), [FEEIT](http://www.feit.ukim.edu.mk), [Ss Cyril and Methodius University in Skopje](http://www.ukim.edu.mk/), Macedonia </li></ul>
