title: evoc-learn
author: evoc-learn
save_as: index.html
url: 


# The evoc-learn Project

We test the hypothesis that children learn by using their own articulators as a learning device to experiment with different vocal manoeuvres until they are understood by adults. 

To this end we rely on state-of-the-art articulatory synthesis to emulate all the critical elements of the learning process, with the goal to generate highly intelligible and natural sounding speech with the learned articulatory parameters.

<script src="http://vjs.zencdn.net/7.20.3/video.js"></script>
<video id="anqi-evoclearn" class="video-js vjs-default-skin" controls preload="auto" width="480" height="270" data-setup="">
<source src="videos/anqi_evoclearn_cvc_cvcv_updated.mp4" type='video/mp4'>
</video>

---

# News

- [Three of our works](https://evoc-learn.gitlab.io/publications) on vocal exploration are currently under review.
- We presented two talks and a Show&Tell at [Interspeech 2022](https://evoc-learn.gitlab.io/events#is2022) in Korea.
- Our [Workshop in Macedonia](https://evoc-learn.gitlab.io/events#workshop) during July 2022 presented different aspects of simulating vocal learning.
