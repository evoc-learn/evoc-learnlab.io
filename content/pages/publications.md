title: evoc-learn Publications
author: evoc-learn
save_as: publications.html
url: publications


# Publications {#publications}

- Krug P.K., Birkholz P., Gerazov B., Van Niekerk D.R., Xu A., and Xu Y., **“Articulatory Synthesis for Data Augmentation in Phoneme Recognition,”** *Proc. Interspeech*, Incheon, South Korea, Sep. 2022. \[[pdf](https://www.isca-speech.org/archive/pdfs/interspeech_2022/krug22_interspeech.pdf)\]

- Van Niekerk D.R., Xu A., Gerazov B., Krug P.K., Birkholz P., and Xu Y., **“Exploration strategies for articulatory synthesis of complex syllable onsets,”** *Proc. Interspeech*, Incheon, South Korea, Sep. 2022. \[[pdf](https://www.isca-speech.org/archive/pdfs/interspeech_2022/vanniekerk22_interspeech.pdf)\]

- Krug P.K., Gerazov B., Van Niekerk D.R, Xu A., Xu Y., and Birkholz P., **“Modelling microprosodic effects can lead to an audible improvement in articulatory synthesis,”** *Journal of the Acoustical Society of America*, vol. 150 (2), pp. 1209-1217, Aug. 2021. \[[pdf](http://www.homepages.ucl.ac.uk/~uclyyix/yispapers/Krug_etAl_JASA2021.pdf)\]

- Xu A., Van Niekerk D.R., Gerazov B., Krug P.K., Prom-on S., Birkholz P., and Xu Y., **“Model-based exploration of linking between vowel articulatory space and acoustic space,”** *Proc. Interspeech*, Brno, Czechia, Sep. 2021.
\[[pdf](https://www.isca-speech.org/archive/pdfs/interspeech_2021/xu21j_interspeech.pdf)\]

- Van Niekerk D.R., Xu A., Gerazov B., Krug P.K., Birkholz P., and Xu Y., **“Finding intelligible consonant-vowel sounds using high-quality articulatory synthesis,”** *Proc. Interspeech*, Shanghai, China, Oct. 2020.
\[[pdf](https://www.isca-speech.org/archive/pdfs/interspeech_2020/niekerk20_interspeech.pdf)\]

- Xu, A., Birkholz, P. and Xu, Y., **“Coarticulation as synchronized dimension-specific sequential target approximation: An articulatory synthesis simulation,”** *Proc. ICPhS*, Melbourne, Australia, Aug. 2019. \[[pdf](https://www.internationalphoneticassociation.org/icphs-proceedings/ICPhS2019/papers/ICPhS_254.pdf)\]

- Xu Y., **“Syllable is a synchronization mechanism that makes human speech possible,”** *PsyArXiv*. \[[pdf](https://psyarxiv.com/9v4hr/download)\]


# Forthcoming {#forthcoming}

- Krug P.K., Birkholz P., Gerazov B., Van Niekerk D.R, Xu A., and Xu Y., **“Artificial Vocal Learning guided by Phoneme Recognition and Visual Information,”** *In review*, Aug. 2022. \[[pdf](https://www.techrxiv.org/articles/preprint/20502114)\]

- Xu A., Van Niekerk D.R., Gerazov B., Krug P.K., Birkholz P., Prom-on S., Halliday L., and Xu Y., **“A computational simulation of human vocal learning,”** *In review*, Jun. 2022.

- Van Niekerk D.R., Xu A., Gerazov B., Krug P.K., Birkholz P., Halliday L., Prom-on S., and Xu Y., **“Simulating vocal learning of spoken language: Beyond imitation,”** *Submitted to Speech Communication*, Jun. 2022.

- Gerazov B., Van Niekerk D.R., Xu A., Krug P.K., Birkholz P., and Xu Y., **“Evaluating Features and Metrics for High-Quality Simulation of Early Vocal Learning of Vowels,”** *ArXiv e-prints*, Apr. 2021. 
\[[pdf](https://arxiv.org/abs/2005.09986)\]
