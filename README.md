# evoc-learn GitLab page

This is the website of the [evoc-learn project](https://evoc-learn.gitlab.io/).

The website is built using [Pelican](https://docs.getpelican.com) a Python based Static Site Generator (SSG), which uses `html` templates to render content written in Markdown `md` (or other) format to a static (not WordPress :) website.

The project (important) folder structure is:

```python
.
├── content
│   ├── images      # images that are included in the webpages
│   │   └── logos   # logos that are included in the webpages
│   ├── audio       # audios that are included in the webpages
│   └── pages       # the webpages in md format
├── my_tuxlite_zf   # theme used to render the html
└── pelicanconf.py  # main config file

2 directories, 8 files

```

## Contributing

### Clone the project

If you added an [SSH key](https://docs.gitlab.com/ee/user/ssh.html) to your GitLab account you can use:

```python
git clone git@gitlab.com:evoc-learn/evoc-learn.gitlab.io.git
```

Otherwise clone it via `https` (it will bother you with entering your GitLab password every time you push):

```python
git clone https://gitlab.com/evoc-learn/evoc-learn.gitlab.io.git
```


### Install Pelican

Do this using:

```python
pip install -r requirements.txt
```

### Generate site locally

Run:
```python
make devserver
```

And open the address in your browser: http://127.0.0.1:8000/

### Add content

#### Edit existing pages

To edit existing webpages you can update the Markdown files in `content/pages/`. The devserver will rebuild the site automatically so you can see the changes by refreshing your browser.

#### Create new pages

To create new webpages you can use one of the existing Markdowns as a template and work from there. 
Here's a small [tutorial](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) about adding headings/lists/images/tables in Markdown as well as one on [linking](https://docs.getpelican.com/en/3.6.3/content.html) in Markdown in Pelican.

The heading of the Markdown tells Pelican important things, here's the heading of `content/pages/publications.md`:
```markdown
title: evoc-learn Publications # title appearing in the tab of the browser
author: evoc-learn             # not so important
save_as: publications.html     # how the html file is saved
url: publications              # how the url for the webpage will appear
```

If you want the new webpage to be listed in the navigation bar at the top of the website it is also important to update `pelicanconf.py`:
```python
PAGES = (
    ('Home', '', 'index.html'),
    ('Project', 'project', 'project.html'),
    ('Publications', 'publications', 'publications.html'),
    ('Team', 'team', 'team.html'),
    ('New name to appear in NavBar', 'link to display in the address', 'file_to_point_to.html'),
    )
```

#### Push changes to GitLab

If you're happy with the way the site looks locally you can now push it to GitLab using:

```python
git add -A
git commit -m "added new content"
git push
```

